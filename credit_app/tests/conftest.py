from pytest_factoryboy import register
from credit_app import factories

register(factories.CreditFactory)
register(factories.CustomerFactory)


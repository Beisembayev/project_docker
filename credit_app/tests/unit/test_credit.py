import pytest
from credit_app import models
from credit_app import factories


def test_credit_factory_returns_valid_credit(db, credit_factory):
    credit = credit_factory()
    assert isinstance(credit, models.Credit)
    assert credit.status == models.Credit.STATUS_CREATED


def test_credit_can_change_status_to_active(db, credit_factory):
    credit = credit_factory.active_credit()
    assert credit.status == models.Credit.STATUS_ACTIVE


def test_credit_can_change_status_to_paid(db, credit_factory):
    credit = credit_factory.paid_credit()
    assert credit.status == models.Credit.STATUS_PAID

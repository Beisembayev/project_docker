from django.db import models
from django_fsm import FSMField, transition


class Timestamp(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Customer(Timestamp):
    last_name = models.CharField(max_length=30, verbose_name="фамилия")
    first_name = models.CharField(max_length=30, verbose_name="имя")
    middle_name = models.CharField(max_length=30, verbose_name="отчество")
    iin = models.CharField(max_length=12, verbose_name="иин")


class Credit(Timestamp):
    STATUS_CREATED = 'created'
    STATUS_ACTIVE = 'active'
    STATUS_PAID = 'paid'
    STATUS_CHOICES = [
        (STATUS_CREATED, 'created'),
        (STATUS_ACTIVE, 'active'),
        (STATUS_PAID, 'paid')
    ]

    MIN_AMOUNT = 5000
    MAX_AMOUNT = 100000

    MIN_DAYS = 5
    MAX_DAYS = 30

    deadline = models.DateTimeField(verbose_name='Время сдачи', null=True)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    status = FSMField(choices=STATUS_CHOICES, default=STATUS_CREATED, verbose_name="статус")

    @transition(field=status, source=STATUS_CREATED, target=STATUS_ACTIVE)
    def to_status_active(self):
        pass

    @transition(field=status, source=STATUS_ACTIVE, target=STATUS_PAID)
    def to_status_paid(self):
        pass

# Create your models here.

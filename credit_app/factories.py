import random
import factory

from datetime import datetime, timedelta
from django.utils import timezone

from . import models


class TimestampFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Timestamp
        abstract = True

    created = datetime.now(tz=timezone.utc)
    modified = created


class CustomerFactory(TimestampFactory):
    class Meta:
        model = models.Customer

    last_name = factory.Faker('last_name')
    first_name = factory.Faker('first_name')
    middle_name = factory.Faker('last_name')

    iin = factory.LazyAttribute(lambda customer: str(random.randrange(100000000000, 1000000000000)))


class CreditFactory(TimestampFactory):
    class Meta:
        model = models.Credit

    customer = factory.SubFactory(CustomerFactory)
    amount = factory.LazyAttribute(
        lambda c: random.randrange(models.Credit.MIN_AMOUNT, models.Credit.MAX_AMOUNT, models.Credit.MIN_AMOUNT)
    )
    deadline = factory.LazyAttribute(
        lambda c: datetime.now(tz=timezone.utc) + timedelta(
            days=(random.randrange(models.Credit.MIN_DAYS, models.Credit.MAX_DAYS + 1)))
    )

    @classmethod
    def active_credit(cls):
        credit = CreditFactory()
        credit.to_status_active()
        return credit

    @classmethod
    def paid_credit(cls):
        credit = CreditFactory.active_credit()
        credit.to_status_paid()
        return credit
